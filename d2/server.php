<?php

session_start();

// method for adding a new task
class TaskList{
    public function add($description){
        $newTask = (object)[
            'description' => $description,
            'isFinished' => false
        ];

        if($_SESSION['tasks'] === null){ //checks if a session variable names tasks exists
            $_SESSION['tasks'] = array(); //if not, create a new one that is 
        };

        array_push($_SESSION['tasks'], $newTask);
    }
}

$taskList = new TaskList(); //create a new taskList object

if($_POST['action'] === 'add'){ //if the action inputs value is "add", call the add method
    $taskList->add($_POST['description']);
}

header('Location: ./index.php'); //redirect the user back to index