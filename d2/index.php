<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP SC S5</title>
</head>
<body>
    <?php session_start(); ?>
    <h1>Add Task</h1>
    <form method="POST" action="./server.php">
        <!-- the value of the "action" attribute dictates where the form data should go -->
        <input type="hidden" name="action" value= "add" >
        Description: <input type="text" name="description" required>
        <button type="submit">Add</button>
    </form>

    <h1>Task List</h1>
    <?php if(isset($_SESSION['tasks'])): ?>
        <?php foreach($_SESSION['tasks'] as $index => $task): // loop through the session array, getting each elements index and the task object ?>
            <div>
                <input type="text" name="description" value="<?= $task->description; ?>">
            </div>

        <?php endforeach; ?>
    <?php endif; ?>
</body>
</html>