<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP SC S5 A1</title>
</head>
<body>
    <?php session_start(); ?>

    <form action="./server.php" method="POST">
        <input type="email" name="email" required>
        <input type="password" name="password" required>
        <button type="submit">Login</button>
    </form>

    <?php if(isset($_SESSION['email'])): ?>
        <p>Hi, <?= $_SESSION['email']; ?></p>

    <?php else: ?>
        <?php if(isset($_SESSION['login_error'])): ?>
            <p><?php $_SESSION['login_error']; ?></p> 
    
        <?php endif; ?>
    <?php endif; ?>
    
</body>
</html>